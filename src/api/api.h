#ifndef _INTERPROTO_H
#define _INTERPROTO_H

/* I remember having this enum starting on 1, but I don't remember why. */

enum {
 MSNP2 = 0
};

enum {
 LI_OK,
 LI_XFR,
 LI_INV,
 LI_ERR   
};

enum {
 NP_OK,
 NP_ERR
};

typedef struct api_connect_arg {
 char *username;
 char *password;
 int protocol;
} api_connect_arg;

int establish_communication(const char *ip_addr);
int negociate_protocol(int socket, int req_protocol);
int authenticate(int socket, const char *username, const char *password,
		 char *return_data);

void api_gather_cl(void);
void api_connect(const char *username, const char *password,
      long int protocol);
void api_set_status(ns_status_t new_status);

#endif
