/*
 *  interproto.c - Protocol API for the msnclient UI.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include "../ui/api.h"
#include "api.h"
#include "../protocols/sia.h"
#include "../uri.h"
#include "../md5/md5str.h"
#include "../wrappers.h"

#define dmesg(msg, buffer) \
  ui_put_message("C:%s. File: %s; Line No: %d\n  Data: %s", \
		 msg, __FILE__, __LINE__, buffer);

/* Caution: I don't know if while one thread is writing to the socket, another
 * one can do it too (as in thr1 sends "abc\n" and thr2 sends "def\n" resulting
 * in something like "abdec\nf\n". In that case we should use mutexes.
 */

/* Caution: All TrID variables must be of type long. I think I have missed
 * some of them, which will most probably have int.
 */

/* Note: For all thread functions used to receive data from the sia, must
 * their identifier must start with "thread_". Also, for all api functions used
 * by the UI to do something about the protocol must start with "api_"
 */

/* In ~/doc/flow.txt I state how I want the program to be modularized in the
 * future. At the moment, all the protocols are (and should be) implemented
 * in this file with if()s. We'll find some way to fix that later. We need
 * a working prototype.
 */

int ns_socket;

long current_protocol;

char *protocol_names[] = {
 "MSNP2"
};

const long protocols_n = 1;

long _TrID = 2;

pthread_t sia_thread;
pthread_t nta_thread;

socket_input_admin_data arg;
int intarg;

void *no_trid_admin(void *arg)
{
 char buffer[2048] = "";
 int buffer_size = 2048;
 do
 {
  wait_from_sia(0, (char *) buffer, buffer_size);
  ui_put_message("%s\n", buffer);
 } while (1);			/* FIXME: This was only to alpha test. */
 return NULL;
}

void start_the_listener()
{

 pthread_create(&nta_thread, NULL, no_trid_admin, NULL);

 arg.socket = ns_socket;
 pthread_create(&sia_thread, NULL, socket_input_admin, &arg);
}

void api_connect(const char *username, const char *password,
      long int protocol)
{
 char return_data[1024] = "64.4.13.17";
 int socket;
 int r;

 do
 {
  socket = establish_communication(return_data);
  ns_socket = socket;
  negociate_protocol(socket, MSNP2);
  r = authenticate(socket, username, password, return_data);
 } while (r == LI_XFR);

 if (r == LI_OK)
 {
  ui_login_ok(return_data);

  start_the_listener();
 }

 if (r == LI_INV)
  ui_login_failed();
 if (r == LI_ERR)
 {
 }
}

int establish_communication(const char *ip_addr)
{
 struct sockaddr_in address;
 int s;

 if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  mabort("msnclient: Error creating socket. socket() returned %d\n", s);

 address.sin_family = AF_INET;
 address.sin_port = htons(1863);
 inet_pton(AF_INET, ip_addr, &address.sin_addr);

 if (connect(s, (struct sockaddr *) &address, sizeof(address)) != 0)
  mabort("Couldn't connect socket! connect() returned: %d\n", errno);

 return s;
}

int negociate_protocol(int socket, int req_protocol)
{
 char buffer[1024];
 int buffer_size = 1024;
 char req_protocol_name[20];

 strcpy(req_protocol_name, protocol_names[req_protocol]);

 sprintf(buffer, "VER %d %s\n", 1, req_protocol_name);
 zsend(socket, buffer, strlen(buffer));
 zrecv(socket, buffer, buffer_size);
 if (!strncmp(buffer, "VER", 3))
 {
  if (!strncmp(buffer + 4, "1", 1))
  {
   if (!strncmp(buffer + 6, req_protocol_name, strlen(req_protocol_name)))
   {
    int i;
    for (i = 0; i < protocols_n; i++)
    {
     if (!strncmp(req_protocol_name, protocol_names[i],
		 strlen(protocol_names[i])))
     {
      current_protocol = i;
     }
    }
   }
   else
    mabort("Couln't negociate protocol. Returned: %s\n", buffer + 6);
  }
  else
   mabort("Couldn't negociate protocol. Returned: %s\n", buffer + 6);
 }
 else
  mabort("Couldn't negociate protocol. Returned: %s\n", buffer + 6);
 return 0;
}

int authenticate(int socket, const char *username, const char *password,
      char *return_data)
{
 char buffer[1024];
 int buffer_size = 1024;
 char challenge[20];

 zsend(socket, "INF 1\n", 6);
 zrecv(socket, buffer, buffer_size);

 if (!strncmp(buffer, "INF", 3))
 {
  if (!strncmp(buffer + 4, "1", 1))
  {
   if (!strncmp(buffer + 6, "MD5", 3))
   {
    // This left intentionally blank. If Microsoft adds a new auth
    // protocol, here we should set a variable or something.
   }
   else
    return LI_ERR;
  }
  else
   return LI_ERR;
 }
 else
  return LI_ERR;

 snprintf(buffer, buffer_size, "USR %d %s I %s\n", 1, "MD5", username);
 zsend(socket, buffer, strlen(buffer));
 zrecv(socket, buffer, buffer_size);

 if (!strncmp(buffer, "USR", 3))
 {
  char hash[80] = "";
  sscanf(buffer + 4, "%*d %*s S %s", challenge);
  md5sum(challenge, password, (char *) &hash);
  sprintf(buffer, "USR %d %s S %s\n", 1, "MD5", hash);
  zsend(socket, buffer, strlen(buffer));
 }
 else if (!strncmp(buffer, "XFR", 3))
 {
  sscanf(buffer + 4, "%*d %*s %[^:]:%*d\n", return_data);
  return LI_XFR;
 }
 else
  return LI_ERR;

 zrecv(socket, buffer, buffer_size);
 if (!strncmp(buffer, "USR", 3))
 {
  if (!strncmp(buffer + 4, "1", 1))
  {
   if (!strncmp(buffer + 6, "OK", 2))
   {
    if (!strncmp(buffer + 9, username, strlen(username)))
    {
     sscanf(buffer + 9 + strlen(username) + 1, "%s\n", return_data);
     strcpy(return_data, (char *) uri_unescape(return_data));
     return LI_OK;
    }
    else
     return LI_ERR;
   }
   else
   {
    return LI_INV;
   }
  }
 }
 else
  return LI_ERR;

 return LI_ERR;
}

int get_trid()
{
 return _TrID++;
}

void *thread_set_status(void *arg)
{
 if (current_protocol == MSNP2)
 {
  long TrID;
  char TrID_char[10];
  char buffer[40];
  int buffer_size = 40;
  TrID = *(long int *) arg;
  sprintf(TrID_char, "%ld", TrID);

  wait_from_sia(TrID, (char *) buffer, buffer_size);

  /* FIXME: Obvious... */
  if (!strncmp(buffer, "CHG ", 4))
  { 
   if (!strncmp(buffer + 4, TrID_char, strlen(TrID_char)))
   {
    if (!strncmp(buffer + 6, "NLN", 3))
     ui_set_status(NSS_ONLINE);
    else if (!strncmp(buffer + 6, "FLN", 3))
     ui_set_status(NSS_OFFLINE);
    else if (!strncmp(buffer + 6, "AWY", 3))
     ui_set_status(NSS_AWAY);
    else if (!strncmp(buffer + 6, "IDL", 3))
     ui_set_status(NSS_IDLE);
    else if (!strncmp(buffer + 6, "BSY", 3))
     ui_set_status(NSS_BUSY);
    else if (!strncmp(buffer + 6, "BRB", 3))
     ui_set_status(NSS_BERIGHTBACK);
    else if (!strncmp(buffer + 6, "PHN", 3))
     ui_set_status(NSS_ONTHEPHONE);
    else if (!strncmp(buffer + 6, "LUN", 3))
     ui_set_status(NSS_OUTTOLUNCH);
    else if (!strncmp(buffer + 6, "HDN", 3))
     ui_set_status(NSS_APPEAROFFLINE);
    else
     dmesg("Unrecognized status", buffer);
   }
  }
  else
   dmesg("Unexpected data", buffer);
 }
 return NULL;
}

void pthread_create_detached(void *(*func) (void *), void *arg)
{
 int n;
 pthread_t thread;

 pthread_attr_t attr;
 pthread_attr_init(&attr);
 pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

 n = pthread_create(&thread, &attr, func, arg);
}

void *thread_receive_cl(void *TrID)
{
 char buffer[2048] = "";
 int buffer_size = 2048;
 do
 {
  wait_from_sia(*(long int *) TrID, (char *) buffer, buffer_size);
  ui_put_message("%s", buffer);
 } while (1);			/* FIXME: This was only to alpha test. */
 return NULL;
}

void api_gather_cl(void)
{
 if (current_protocol == MSNP2)
 {
  char buffer[25];
  long int buffer_size = 25;
  long int TrID = get_trid();

  /* Start the CL receiver */

  pthread_create_detached(thread_receive_cl, &TrID);

  snprintf(buffer, buffer_size, "LST %ld FL\n", TrID);
  zsend(ns_socket, buffer, strlen(buffer));
 }
}

void api_set_status(ns_status_t new_status)
{
 if (current_protocol == MSNP2)
 {
  int TrID;

  TrID = get_trid();
  if (new_status == NSS_ONLINE)
  {
   char buffer[40];
   int buffer_size = 40;
   intarg = TrID;
   pthread_create_detached(thread_set_status, &intarg);
   snprintf(buffer, buffer_size, "CHG %d NLN\n", TrID);
   zsend(ns_socket, buffer, strlen((char *) buffer));
  }
  else if (new_status == NSS_APPEAROFFLINE)
  {
   char buffer[40];
   int buffer_size = 40;
   intarg = TrID;
   pthread_create_detached(thread_set_status, &intarg);
   snprintf(buffer, buffer_size, "CHG %d HDN\n", TrID);
   zsend(ns_socket, buffer, strlen((char *) buffer));
  }
  else if (new_status == NSS_OFFLINE)
  {
   char buffer[40];
   int buffer_size = 40;
   intarg = TrID;
   pthread_create_detached(thread_set_status, &intarg);
   snprintf(buffer, buffer_size, "CHG %d FLN\n", TrID);
   zsend(ns_socket, buffer, strlen((char *) buffer));
  }
  else if (new_status == NSS_AWAY)
  {
   char buffer[40];
   int buffer_size = 40;
   intarg = TrID;
   pthread_create_detached(thread_set_status, &intarg);
   snprintf(buffer, buffer_size, "CHG %d AWY\n", TrID);
   zsend(ns_socket, buffer, strlen((char *) buffer));
  }
 }
}
