/*
 *  wrappers.c - Some wrappers for msnclient.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */

#include "wrappers.h"
#include "stdarg.h"
void remove_returns(char str[], long int str_size)
{
 long int from;
 long int to=0;
 for (from=0;from<str_size && str[from]!=0;from++)
  if (str[from] != '\r')
  {
   str[to] = str[from];
   to++;
  }
 str[to] = '\0'; 
}

void flog(const char *format, ...)
{
 va_list ap;
 FILE *log;
 va_start(ap, format);
 log = fopen("log", "a");
 vfprintf(log, format, ap);
 fclose(log);
}

int qsend(int socket, char *data, int size, int flags)
{
 int e;
 e = send(socket, data, size, flags);
 
 flog("s<-%s\n", data);
 if (e != size)
 {
  flog("msnclient: Error while sending data to socket! Exiting.\n");
  exit(10);
 }
 return e;
}

int qrecv(int socket, char *buff, int size, int flags)
{
 int e;
 memset(buff, 0, size);
 e = recv(socket, buff, size, flags);
 if (e >= size)
 {
  flog("Next message was longer than the buffer: %d\n", e);
 }
 flog("s->%s\n", buff);
 if (e < 0)
 {
  flog("msnclient: Error while receiving from socket! Error: %d. Exiting\n.", e);
  exit(10);
 }
 remove_returns(buff, size);
 return e;
}


