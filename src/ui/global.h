#ifndef _UI_GLOBAL_H
#define _UI_GLOBAL_H

#include "api.h"

typedef struct contact {
 char handle[1024];
 char friendly[1024];
 char alias[20]; /* This an alias associated for use with msnclient */
 char phone[1024];
 char mobile[1024];
} contact_t;

bool connected;
char friendly[1024];
ns_status_t ns_status;

WINDOW * mainw;
WINDOW * promptw;
WINDOW * statusw;
WINDOW * borderw;

#endif /*_UI_GLOBAL_H */
