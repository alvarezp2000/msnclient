/*
 *  hands.c - User input manager module.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ncurses.h>
#include <pthread.h>
#include "../wrappers.h"
#include "global.h"
#include "api.h"
#include "../api/api.h"

typedef struct thread_data_t
{
 pthread_t * thread_id;
 void *more_data;
} thread_data_t;

thread_data_t *pthreads=NULL;
int pthreads_n=0;

void user_input_admin()
{
 char buffer[1024];
 int buffer_size=1024;
 do
 {
  wprintw(promptw, ": "); wrefresh(promptw);
  wgetnstr(promptw, buffer, buffer_size);
  if (!strncmp(buffer, "/connect", 8))
  {
   char username[80];
   char password[80];
   
   sscanf(buffer+9, "%s\n", username);
   wprintw(promptw, "Password: "); wrefresh(promptw);
   noecho();
   wgetnstr(promptw, password, 79);
   echo();
   wprintw(promptw, "\n"); wrefresh(promptw);
   wprintw(mainw, "C:Trying to log in...\n"); wrefresh(mainw);
   ns_status = 0;
   api_connect(username, password, MSNP2);
  }
  if (!strcmp(buffer, "/online"))
  {
   if (connected)
   {
    if (ns_status != NSS_ONLINE)
    {
     api_set_status(NSS_ONLINE);
    }
    else
    {
     wprintw(mainw, "C:You are already online!\n"); wrefresh(mainw);
    }
   }
   else
   {
    wprintw(mainw, "C:You are not connected.\n"); wrefresh(mainw);
   }
  }
  if (!strcmp(buffer, "/offline"))
  {
   if (connected)
   {
    if (ns_status != NSS_APPEAROFFLINE)
    {
     api_set_status(NSS_APPEAROFFLINE);
    }
    else
    {
     wprintw(mainw, "C:You are already offline!\n"); wrefresh(mainw);
    }
   }
   else
   {
    wprintw(mainw, "C:You are not connected.\n"); wrefresh(mainw);
   }
  }
  if (!strcmp(buffer, "/away"))
  {
   if (connected)
   {
    if (ns_status != NSS_AWAY)
    {
     api_set_status(NSS_AWAY);
    }
    else
    {
     wprintw(mainw, "C:You are already away!\n"); wrefresh(mainw);
    }
   }
   else
   {
   }
  }
  if (!strncmp(buffer, "/quot ", 6))
  {
  }
  if (!strncmp(buffer, "/repaint", 8))
  {
   redrawwin(mainw);
   redrawwin(statusw);
   redrawwin(promptw);
  }
  if (!strncmp(buffer, "/nick ", 6))
  {
   if (connected)
   {
     
   }
  }
  if (!strncmp(buffer, "/test", 6))
  {
   ui_put_message("C:Testing... %d\n", random());
  }
 }while(strcmp(buffer, "/quit"));
}

