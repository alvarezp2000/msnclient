#ifndef _UI_API_H
#define _UI_API_H

typedef enum ns_status_t {
 NSS_DISCONNECTED,
 NSS_OFFLINE,
 NSS_ONLINE,
 NSS_BERIGHTBACK,
 NSS_OUTTOLUNCH,
 NSS_BUSY,
 NSS_AWAY,
 NSS_IDLE,
 NSS_APPEAROFFLINE,
 NSS_ONTHEPHONE
} ns_status_t;

void ui_login_failed();
void ui_login_ok(const char *nick);
void ui_set_status(ns_status_t status);
void ui_put_message(const char *format, ...);
void init_screen();
void user_input_admin();

#endif /* UI_API_H */
