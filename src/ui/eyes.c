/*
 *  eyes.c - Feedback to user manager and API.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */
#include <ncurses.h>
#include <string.h>
#include "global.h"
#include "../api/api.h"
#include <stdarg.h>

WINDOW * contactw;

/* FIXME: There should be a lot of variables to set the current session state
 * like "nick" and stuff
 */ 

contact_t *fl = NULL;
long fl_n = 0;
char nick[1024];
long nick_n = 1024;

const char * ns_status_names[] =
{
 "Online",
 "Be right back",
 "Out to lunch",
 "Busy",
 "Away",
 "Appear offline",
 "On the phone"
};

void lay_screen()
{
 mainw = newwin(23, 63, 0, 0);
 statusw = newwin(1, 80, 23, 0);
 promptw = newwin(1, 80, 24, 0);
 contactw = newwin(23, 15, 0, 66);
 borderw = newwin(23, 1, 0, 65);
 idlok(mainw, TRUE);
 scrollok(mainw, TRUE);
 idlok(promptw, TRUE);
 scrollok(promptw, TRUE);
 idlok(statusw, TRUE);
 wprintw(mainw, "---SESSION START---\n");
 wprintw(mainw, "Type /connect <email_address> to sign in.\n");
 wattron(statusw, A_REVERSE);
 wprintw(statusw, "                                                                 |Disconnected");
 mvwvline(borderw, 0, 0, '|', 23);
 wrefresh(mainw);
 wrefresh(statusw);
 wrefresh(contactw);
 wrefresh(borderw);
}

void ui_set_nick(const char *new_nick)
{
 strcpy(nick, new_nick);
 mvwprintw(statusw, 0, 0, "%s", new_nick);
 wrefresh(statusw);
}

void ui_add_contact_to_fl(const char *handle, const char *friendly,
			  long int new_version)
{
 
}

void ui_rmv_contact_from_fl(const char *handle, long int new_version)
{
}

void ui_incoming_msg(const char *handle, const char *data)
{
}

void ui_nak_msg(const char *handle, const char *data)
{
}

void ui_set_status(ns_status_t status)
{
 switch (status)
 {
 case NSS_ONLINE:
  mvwprintw(statusw, 0, 65, "|Online        "); break;
 case NSS_BERIGHTBACK:
  mvwprintw(statusw, 0, 65, "|Be Right Back "); break;
 case NSS_OUTTOLUNCH:
  mvwprintw(statusw, 0, 65, "|Out to lunch  "); break;
 case NSS_BUSY:
  mvwprintw(statusw, 0, 65, "|Busy          "); break;
 case NSS_AWAY:
  mvwprintw(statusw, 0, 65, "|Away          "); break;
 case NSS_APPEAROFFLINE:
  mvwprintw(statusw, 0, 65, "|Appear offline"); break;
 case NSS_ONTHEPHONE:
  mvwprintw(statusw, 0, 65, "|On the phone  "); break;
 case NSS_OFFLINE:
  mvwprintw(statusw, 0, 65, "|Just connected"); break;
 case NSS_DISCONNECTED:
  mvwprintw(statusw, 0, 65, "|Disconnected  "); break;
 default:
  break;
 }
 wrefresh(statusw);
 ns_status = status;
}

void init_screen()
{
 /* 1. Prepare screen output. */
 lay_screen();

 /* 2. Set initial session parameters. */
 ui_set_nick("");
 connected = 0;
}

void ui_login_ok(const char *nick)
{
 wprintw(mainw, "Login succeded.\n");
 wrefresh(mainw);
 ui_set_nick(nick);
 connected = 1;
 api_gather_cl();
}

void ui_login_failed()
{
 wprintw(mainw, "Login failed. Try /connect-ing again.\n");
 wrefresh(mainw);
 connected = 0;
}

void ui_put_message(const char *format, ...)
{
 va_list ap;
 va_start(ap, format);
 vwprintw(mainw, (char *)format, ap);
 wrefresh(mainw);
 va_end(ap);
}
