/*
 *  sia.c - Socket Input Administrator. A little bit of magic. :-)
 *          It reads data from the socket and distributes it according to
 *          the TrID.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */

#include <pthread.h>
#include "sia.h"
#include "../wrappers.h"

/* CAUTION: All buffer_size-like variables throughout the project should be
 * declared as long. I may have missed some already by now.
 */

/* Variables de control de sia */

/* INICIO */
struct trid_relation_element
{
 int TrID;
 char *buffer;
 long buffer_size;
 pthread_cond_t *condition_variable; 
};

struct trid_relation_element *trid_relation_elements = NULL;
int trid_relation_elements_n = 0;

/* FIN */

/* This will tell sia that someone is waiting. */
void add_trid_to_sia(long TrID, char *buffer, long buffer_size,
		     pthread_cond_t *condition_variable)
{
 /*set_sia_conf(SIA_CMD_ADD, TrID, &tr_cmds);
 
         * As a reminder...
         * struct trid_relation_element
         * {
         *  int TrID;
         *  char *buffer;
         *  long buffer_size;
         *  pthread_cond_t condition_variable; 
         * };
         */
 list_add_element(trid_relation_elements, trid_relation_elements_n,
		  struct trid_relation_element);
 trid_relation_elements[trid_relation_elements_n - 1].TrID = TrID;
 trid_relation_elements[trid_relation_elements_n - 1].buffer = buffer;
 trid_relation_elements[trid_relation_elements_n - 1].buffer_size = buffer_size;
 trid_relation_elements[trid_relation_elements_n - 1].condition_variable = 
   condition_variable;
}

/* Wait from sia is called from any non-sia thread waiting for sia to
 * send anything received from the socket, according to the TrID.
 */
void wait_from_sia(long TrID, char *buffer, long buffer_size)
{
 pthread_cond_t got_request = PTHREAD_COND_INITIALIZER;
 pthread_mutex_t request_mutex = PTHREAD_MUTEX_INITIALIZER;

 add_trid_to_sia(TrID, buffer, buffer_size, &got_request);
 
 pthread_mutex_lock(&request_mutex);
 pthread_cond_wait(&got_request, &request_mutex);
 pthread_mutex_unlock(&request_mutex);

}

void timedwait_from_sia(long TrID, char *buffer, long buffer_size,
			time_t timeout)
{
 pthread_cond_t got_request = PTHREAD_COND_INITIALIZER;
 pthread_mutex_t request_mutex = PTHREAD_MUTEX_INITIALIZER;

 struct timespec abstime;
 abstime.tv_sec = time(NULL)+timeout;
 abstime.tv_nsec = 0;

 add_trid_to_sia(TrID, buffer, buffer_size, &got_request);
 
 pthread_mutex_lock(&request_mutex);
 pthread_cond_timedwait(&got_request, &request_mutex, &abstime);
 pthread_mutex_unlock(&request_mutex);

}
/* THIS MIGHT NOT BE NEEDED. Check...
void sig_pipe(int s)
{
 printf("GOT SIGPIPE!\n");
 abort(); 
}
*/

/* This is the famous "sia" thread. Its function is to monitor the socket
 * for any input data and send it to the appropriate thread, according to
 * the TrID from the input data, or to the nta (no_trid_admin) thread.
 */

void *socket_input_admin(void *arg)
{
 int i;

 /* "data" will be an alias to "arg", but already casted. */
 socket_input_admin_data *data = (socket_input_admin_data *) arg;

 const int socket_buffer_size = 2048;
 char socket_buffer[2048];
 memset(socket_buffer, 0, socket_buffer_size);
 int s_trid = 0;

 int e;
 do
 {
  e=zrecv(data->socket, (char *)socket_buffer, socket_buffer_size); 
  
  if (strlen(socket_buffer) != 0)
  {
   if (strlen(socket_buffer) > 0)
   {
    /* Check to see if it has TrID */
    char s_strid1[30] = "";
    char s_strid2[30] = "";

    sscanf(socket_buffer, "%*3s %s %*[^\n]\n", s_strid1);
    s_trid = atoi(s_strid1);
    sprintf(s_strid2, "%d", s_trid);
    if (strcmp(s_strid1, s_strid2)) /* Second argument is not an integer */
     s_trid = 0;

    /* We are sure that we have a TrID, even if the message had no TrID, in
     * which case, now is zero.
     */
    for (i = trid_relation_elements_n - 1;
          (i >= 0) && (trid_relation_elements[i].TrID != s_trid); i--);

    if (i>=0)
    {
     strncpy(trid_relation_elements[i].buffer, socket_buffer,
	     trid_relation_elements[i].buffer_size);
     pthread_cond_signal(trid_relation_elements[i].condition_variable);

     memcpy(&trid_relation_elements[i], &trid_relation_elements[i+1],
	    sizeof(struct trid_relation_element)*(trid_relation_elements_n - i
						  - 1)); 
     list_rmv_element(trid_relation_elements, trid_relation_elements_n,
		      struct trid_relation_element);
     
    }
    strcpy(socket_buffer, "");
   }
  }
 }while(e!=0);
 free(trid_relation_elements);
 trid_relation_elements_n = 0;
 return NULL;
}

/* THIS SHOULD NO LONGER BE NEEDED
// This is used to send commands to sia. It is recommended not to use this
// function from a module other than this. Check add_trid_to_sia and
// rmv_trid_to_sia for interfaced functions.
void set_sia_conf(int cmd, int t_trid, strline * tr_cmds)
{
 int i;
 switch (cmd)
 {
 case SIA_CMD_ADD:
  list_add_element(trid_relation_elements, trid_relation_elements_n,
        struct trid_relation_element);
  trid_relation_elements[trid_relation_elements_n - 1].trid = t_trid;
  trid_relation_elements[trid_relation_elements_n - 1].tr_cmds = *tr_cmds;
  break;
 case SIA_CMD_RMV:
  for (i = trid_relation_elements_n - 1;
        (i >= 0) && (trid_relation_elements[i].trid != t_trid); i--);
  trid_relation_elements[i].trid =
        trid_relation_elements[trid_relation_elements_n - 1].trid;
  list_rmv_element(trid_relation_elements, trid_relation_elements_n,
        struct trid_relation_element);
  break;
 default:
  break;
 }
}
*/

/*
 *  THIS SHOULD NO LONGER BE NEEDED
// This will tell sia to stop a TrID-thread relation.
void rmv_trid_to_sia(int TrID)
{
 set_sia_conf(SIA_CMD_RMV, TrID, 0);
}
*/
