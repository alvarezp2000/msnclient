#ifndef _SIA_H
#define _SIA_H

typedef struct socket_input_admin_data
{
 int socket;
} socket_input_admin_data;

void wait_from_sia(long TrID, char *buffer, long buffer_size);
void *socket_input_admin(void *arg);

#endif
