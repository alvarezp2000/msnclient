/* 
   Modified for g++ compatibility by Octavio Alvarez.

   HTTP URI handling
   Copyright (C) 1999-2000, Joe Orton <joe@orton.demon.co.uk>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
   MA 02111-1307, USA

*/

#ifndef URI_H
#define URI_H

#include "neon_defs.h" 

BEGIN_NEON_DECLS

/* Un-escapes a URI. Returns malloc-allocated URI on success,
 * or NULL on failure (malloc failure or invalid %<HEX><HEX> sequence). */
char *uri_unescape(const char *uri);

/* Escapes the abspath segment of a URI.
 * Returns malloc-allocated string on success, or NULL on malloc failure.
 */
char *uri_abspath_escape(const char *abs_path);

END_NEON_DECLS

#endif /* URI_H */
