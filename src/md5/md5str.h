#ifndef _MD5STR_H
#define _MD5STR_H

#include <stdio.h>
#include <string.h>
#include "md5.h"

void md5sum(const char *challenge, const char *password, char *encr);

#endif
