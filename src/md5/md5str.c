/*
 *  md5str.c - A wrapper to use the md5 library a little bit easier
 *             (a la md5sum)
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */

#include "md5str.h"

void md5sum(const char *challenge, const char *password, char *encr)
{
 int i;
 
 md5_state_t pms;
 md5_byte_t digest[16];
   
 md5_init(&pms);
 md5_append(&pms, (const md5_byte_t *)challenge, strlen(challenge));
 md5_append(&pms, (const md5_byte_t *)password, strlen(password));
 md5_finish(&pms, digest);
 for (i=0;i<16;++i) sprintf(encr+i*2, "%02x", digest[i]);
}

