/*
 *  msnclient - A text-console client for MSN Messenger*.
 *  Copyright (C) 2002  Octavio Alvarez Piza.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the version 2 of the GNU General Public License
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *  (Under the msnclient source code as it is in the CVS tree, look under
 *   the ./doc directory. It should be there.)
 *
 *  The author can be contacted via e-mail at alvarezp@sourceforge.net.
 *
 */

#include <ncurses.h>
#include "ui/api.h"

int main(int argc, char *argv[])
{
 initscr();
 init_screen();
 user_input_admin();
 endwin();
 return 0;
}
