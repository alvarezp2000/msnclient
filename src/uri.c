 /* 
    HTTP URI handling
    Copyright (C) 1999-2000, Joe Orton <joe@orton.demon.co.uk>
 
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
    MA 02111-1307, USA
 
 */
 
 #include <string.h>
 #include <unistd.h>
 #include <stdlib.h>
 #include <ctype.h>
 #include <stdio.h>
 
 #include "uri.h" 
 #include "ne_alloc.h" 
 
 /* Un-escapes a URI. Returns ne_malloc-allocated URI */
 char *uri_unescape(const char *uri) 
 {
     const char *pnt;
     char *ret, *retpos, buf[5] = { "0x00" };
     retpos = ret = (char *)ne_malloc(strlen(uri) + 1);
     for (pnt = uri; *pnt != '\0'; pnt++) {
        if (*pnt == '%') {
            if (!isxdigit((unsigned char) pnt[1]) || 
                !isxdigit((unsigned char) pnt[2])) {
                /* Invalid URI */
                return NULL;
            }
            buf[2] = *++pnt; buf[3] = *++pnt; /* bit faster than memcpy */
            *retpos++ = strtol(buf, NULL, 16);
        } else {
            *retpos++ = *pnt;
        }
     }
     *retpos = '\0';
     return ret;
 }
 
 /* RFC2396 spake:
  * "Data must be escaped if it does not have a representation 
  * using an unreserved character".
  */
 
 /* Lookup table: character classes from 2396 */
 
 #define SP 0   /* space    = <US-ASCII coded character 20 hexadecimal>                 */
 #define CO 0   /* control  = <US-ASCII coded characters 00-1F and 7F hexadecimal>      */
 #define DE 0   /* delims   = "<" | ">" | "#" | "%" | <">                               */
 #define UW 0   /* unwise   = "{" | "}" | "|" | "\" | "^" | "[" | "]" | "`"             */
 #define MA 1   /* mark     = "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"       */
 #define AN 2   /* alphanum = alpha | digit                                             */
 #define RE 2   /* reserved = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | "," */
 
 static const char uri_chars[128] = {
 /*                +2      +4      +6      +8     +10     +12     +14     */
 /*   0 */ CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO,
 /*  16 */ CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO, CO,
 /*  32 */ SP, MA, DE, DE, RE, DE, RE, MA, MA, MA, MA, RE, RE, MA, MA, RE,
 /*  48 */ AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, RE, RE, DE, RE, DE, RE,
 /*  64 */ RE, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN,
 /*  80 */ AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, UW, UW, UW, UW, MA,
 /*  96 */ UW, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN,
 /* 112 */ AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, AN, UW, UW, UW, MA, CO 
 };
 
 #define ESCAPE(ch) (((signed char)(ch) < 0 || uri_chars[(unsigned int)(ch)] == 0))
 
 #undef SP
 #undef CO
 #undef DE
 #undef UW
 #undef MA
 #undef AN
 #undef RE
 
 /* Escapes the abspath segment of a URI.
  * Returns ne_malloc-allocated string.
  */
 char *uri_abspath_escape(const char *abs_path) 
 {
     const signed char *pnt;
     char *ret, *retpos;
     int count = 0;
     for (pnt = (const signed char *)abs_path; *pnt != '\0'; pnt++) {
        if (ESCAPE(*pnt)) {
            count++;
        }
     }
     if (count == 0) {
        return ne_strdup(abs_path);
     }
     /* An escaped character is "%xx", i.e., two MORE
      * characters than the original string */
     retpos = ret = (char *)ne_malloc(strlen(abs_path) + 2*count + 1);
     for (pnt = (const signed char *)abs_path; *pnt != '\0'; pnt++) {
        if (ESCAPE(*pnt)) {
            /* Escape it - %<hex><hex> */
            sprintf(retpos, "%%%02x", (unsigned char) *pnt);
            retpos += 3;
        } else {
            /* It's cool */
            *retpos++ = *pnt;
        }
     }
     *retpos = '\0';
     return ret;
 }
 
 #undef ESCAPE
