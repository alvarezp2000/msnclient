#ifndef _WRAPPERS_H
#define _WRAPPERS_H

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#define unexp(m) {printf("Unexpected error!\n%s", m); exit(1);}
#define mabort(m, n) {printf(m, n); exit(1);}

#define list_add_element(x, xn, y) {x = (y *)realloc(x, (++(xn))*sizeof(y));}
#define list_rmv_element(x, xn, y) {x = (y *)realloc(x, (--(xn))*sizeof(y));}

#define ngets(a, b) fgets(a, b, stdin)
#define gets(a) ngets(a, a##_size)

int qrecv(int socket, char *buff, int size, int flags);
int qsend(int socket, char *data, int size, int flags);
/* void set_protection_mutex(pthread_mutex_t *m); */

/* Wrappers for send and recv: */
#define zsend(a,b,c) (qsend(a,b,c,0))
#define zrecv(a,b,c) (qrecv(a,b,c,0))

#endif /* _WRAPPERS_H */

